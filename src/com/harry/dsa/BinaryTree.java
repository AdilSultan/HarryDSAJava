package com.harry.dsa;
class BNode{
	int data;
	BNode left;
	BNode right;
}

public class BinaryTree {
	
	public static BNode create(int data) {
		BNode bnode = new BNode();
		bnode.data = data;
		bnode.left = null;
		bnode.right = null;
		return bnode;
	}

	public static void main(String[] args) {
		
		BNode bnode1 = create(10);
		BNode bnode2 = create(20);
		BNode bnode3 = create(30);
		
		bnode1.left = bnode2;
		bnode1.right = bnode3;

	}

}
