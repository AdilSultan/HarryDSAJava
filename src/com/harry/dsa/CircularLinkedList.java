package com.harry.dsa;
class CNode{
	int data;
	CNode next;
}

public class CircularLinkedList {
	static CNode head;
	
	static void addFirst(int data) {
		CNode cnode = new CNode();
		cnode.data=data;
		
		CNode tempH = head;
		CNode tempHN = tempH.next;
		while(tempHN!=head) {
			tempH = tempH.next;
			tempHN = tempHN.next;
		}	
		cnode.next=head;
		tempH.next=cnode;
		head=cnode;
	}
	
	static void addLast(int data) {
		CNode cnode = new CNode();
		cnode.data=data;
		
		CNode tempH = head;
		CNode tempHN = tempH.next;
		while(tempHN!=head) {
			tempH = tempH.next;
			tempHN = tempHN.next;
		}
		cnode.next = tempH.next;
		tempH.next = cnode;
	}
	
	static void addInMiddle(int index,int data) {
		CNode cnode = new CNode();
		cnode.data = data;
		if(index<=0) {
			System.out.println("Given postion is not valid");
		}
		else if(index==1) {
			addFirst(data);
		}else {
		
			CNode temp = head;
		for(int i=1;i<index-1;i++) {
			temp=temp.next;
		}
		
		cnode.next=temp.next;
		temp.next = cnode;
		
		}
		
	}
	
	public static void main(String[] args) {
		
		CNode cnode1 = new CNode();
		cnode1.data=10;
		head=cnode1; 
		
		CNode cnode2 = new CNode();
		cnode2.data=20;
		cnode1.next=cnode2;
		
		CNode cnode3 = new CNode();
		cnode3.data=30;
		cnode2.next=cnode3;
		
		CNode cnode4 = new CNode();
		cnode4.data=40;
		cnode3.next=cnode4;
		
		cnode4.next = cnode1;
		
		display();
		
		System.out.println("===============");
		addInMiddle(2,100);
		
		display();

	}
	
	static void display() {
		CNode temp = head;	
		do {
			System.out.println(temp.data);
			temp=temp.next;
		}while(temp!=head);
		
	}

}
