package com.harry.dsa;

public class CircularQueue {
	static int maxSize = 10;
	static int front = -1;
	static int rear = -1;
	
	static int[] queue = new int[maxSize];
	
	static void enqueue(int value) {
		if(rear!=-1 && (rear+1)%maxSize==0) {
			System.out.println("Queue is full !");
		}else {
			 rear = (rear+1)%maxSize;
			queue[rear] = value;
		}
		
	}
	
	static void dequeue() {
		if(front==rear) {
			System.out.println("Queue is Empty !");
		}else {
			System.out.println(queue[++front]);
		}
			
	}
	
	static void display() {
		for(int element : queue) {
			System.out.println(element);
		}
	}

	public static void main(String[] args) {
		
		
		
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		enqueue(40);
		
		
	}

}
