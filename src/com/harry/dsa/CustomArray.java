package com.harry.dsa;

public class CustomArray {	
	
	static void display(int [] arr,int size) {
		for(int i=0;i<size;i++){
			System.out.println(arr[i]);
		}
	}

	//insert
	static boolean insert(int [] arr,int size,int element,int indexPosition) {
		
		for(int i=size-1;i>=indexPosition ; i--) {
			
			arr[i+1] = arr[i];
			
		}
		arr[indexPosition] = element;
		
		
		return false;
	}
	
	
	//delete
	static void delete(int[] arr,int size,int indexPostion) {
		
		for(int i=indexPostion; i<=size-1;i++) {
			arr[i]=arr[i+1];
		}
		
	}
	
	public static void main(String[] args) {
		 int capacity=100;
		 int size = 0;
		 int[] arr = new int[capacity];
		size=5;
		for(int i=0;i<5;i++) {
			arr[i]=i;
		}
		display(arr,size);
		insert(arr,size,20,2);
		size++;
		System.out.println("after insert display");
		display(arr,size);
		delete(arr,size,3); size--; System.out.println("after delete display");
		display(arr,size);
		 
		
		
	}

}
