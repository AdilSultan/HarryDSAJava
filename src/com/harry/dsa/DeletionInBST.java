package com.harry.dsa;
class DBNode{
	int data;
	DBNode left;
	DBNode right;	
}
public class DeletionInBST {
	
	static DBNode create(int value) {
		DBNode node = new DBNode();
		node.data=value;
		node.left = null;
		node.right = null;
		return node;
	}
	
	static void inOrder(DBNode root) {
		if(root==null)
			return;
		if(root!=null) {
		inOrder(root.left);
		System.out.print(root.data);
		inOrder(root.right);
		}
	}

	
	public static void main(String[] args) {
		
		DBNode node1 = create(5);
		DBNode node2 = create(6);
		DBNode node3 = create(3);
		DBNode node4 = create(1);
		DBNode node5 = create(4);
		
		node1.right = node2;
		node1.left = node3;
		
		node3.right = node5;
		node3.left = node4;
		
		inOrder(node1);
		deleteNode(node1,4);
		System.out.println();
		inOrder(node1);

	}

	private static DBNode deleteNode(DBNode root,int delValue) {
		DBNode inPre = null;
		if(root==null)
			return null;
		if(root.left==null & root.right==null) {
			root.data = 0;
			return null;
		}
		if(root.data > delValue) {
			deleteNode(root.left,delValue);
		}else if(root.data < delValue) {
			deleteNode(root.right,delValue);
		}else {
			inPre = findInPre(root);
			root.data = inPre.data;
			deleteNode(root.left,inPre.data);
				
		}
		return null;
	}

	private static DBNode findInPre(DBNode root) {
		root = root.left;
		
		while(root.right!=null) {
			root = root.right;
		}
		
		return root;
	}

}
