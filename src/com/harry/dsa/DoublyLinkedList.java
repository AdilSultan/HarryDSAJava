package com.harry.dsa;
class DNode{
	DNode previous;
	int data;
	DNode next;
}

public class DoublyLinkedList {

	static DNode head;
	
	static void readLeftToRight() {
		
		DNode temp = head;
		while(temp!=null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
		
	}
	
	static void fromRightToLeft() {
		
		DNode temp = head;
		while(temp.next!=null) {
			temp = temp.next;
		}
		
		while(temp!=head) {
			System.out.println(temp.data);
			temp = temp.previous;
			
		}
		System.out.println(temp.data);
		
	}
	
	
	public static void main(String[] args) {
		
		DNode node1 = new DNode();
		node1.data = 10;
		head=node1;
		
		
		DNode node2 = new DNode();
		node2.data =20;
		node1.next = node2;
		node2.previous=node1;
		
		DNode node3=new DNode();
		node3.data = 30;
		node2.next = node3;
		node3.previous = node2;
		
		DNode node4 = new DNode();
		node4.data = 40;
		node3.next = node4;
		node4.previous = node3;
		
		//readLeftToRight();
		fromRightToLeft();
		
	}

}
