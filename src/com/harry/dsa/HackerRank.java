package com.harry.dsa;

public class HackerRank {

	public static void main(String[] args) {
		String[] lapTimes = {"00000000","01001110","01000011"};
		int totalTime = 0;
		for (String temp : lapTimes) {
			int decimal=Integer.parseInt(temp,2);  
			totalTime+=decimal;	
        }
		if(totalTime!=0 && totalTime!=255)   
	    System.out.println(
	   String.format("%02d:%02d:%02d", totalTime / 3600, (totalTime % 3600) / 60, (totalTime % 60)));

	}

}
