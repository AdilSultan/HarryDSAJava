package com.harry.dsa;

public class InsertNodeInBTS {
	
	static BNode create(int value) {
		BNode node = new BNode();
		node.data = value;
		node.left = null;
		node.right = null;
		return node;
	}
	
public static BNode insert(BNode root,int insert) {
		
		if(root==null) {
			return null;
		}
		BNode prev = null;
		 while(root!=null) {
			 
			 prev = root;
			 if(root.data==insert) {
				 return null;
			 }
			 else if(root.data > insert) {
				 root = root.left;
			 }else {
				 root = root.right;
			 }
		 }
		 BNode newNode = create(insert);
		 if(prev.data > insert)
			 prev.left = newNode;
		 else
			 prev.right = newNode;
		 
		 
		return newNode;
		
	}

	public static void main(String[] args) {
		
		BNode node1= create(50);
		BNode node2= create(40);
		BNode node3= create(20);
		BNode node4= create(45);
		BNode node5= create(60);
		BNode node6= create(55);
		BNode node7= create(70);
		
		node1.left = node2;
		node1.right = node5;
		
		node2.left = node3;
		node2.right = node4;
		
		node5.left = node6;
		node5.right = node7;
		
		BNode found = insert(node1, 80);
		
		
		
		int data = node1.right.right.right.data;
		
		System.out.println(data);

	}

}
