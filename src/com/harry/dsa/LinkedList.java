package com.harry.dsa;

class Node {
	int data;
	Node next;

	public Node() {
		this.next = null;
	}
}

public class LinkedList {

	static Node head;
	static Node tail;

	static void addFirst(int value) {
		Node node = new Node();
		node.next = head;
		node.data = value;
		head = node;
	}

	// by default it will add at last
	static void add(int value) {
		Node node = new Node();
		if (head == null) {
			node.data = value;
			head = node;
			tail = head;
		} else {
			tail.next = node;
			node.data = value;
			tail = node;
		}
	}

	static void addInMiddle(int value, int index) {
		if (index == 0) {
			System.out.println("Postion 0 is not valid !");
		} else if (index == 1) {
			addFirst(value);
		} else if (index > 1) {
			Node node = new Node();
			Node temp = head;
			for (int i = 1; i < index - 1; i++) {
				temp = temp.next;
			}
			node.data = value;
			node.next = temp.next;
			temp.next = node;
		}

	}
	
		static void deleteFirst() {
			head = head.next;
		}
	
		static void deleteInMiddle(int index) {
			
			if (index == 0) {
				System.out.println("Postion 0 is not valid !");
			} else if (index == 1) {
				deleteFirst();
			} else if (index > 1) {
				Node temp = head;
				for (int i = 1; i < index - 1; i++) {
					temp = temp.next;
				}
				temp.next = temp.next.next;
			}
				
		}
		
		static void deleteLast() {
			
			Node current = head;
			Node cnext = current.next;
			while(cnext.next!=null) {
				current = current.next;
				cnext = cnext.next;
			}
			
			current.next=null;
			
		}

	public static void main(String[] args) {

		add(10);
		add(20);
		add(30);
		add(40);

		addInMiddle(15, 3);

		System.out.println("============");

		display();
		
		System.out.println("============");
		
		deleteLast();
		display();

	}

	static void display() {

		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

}
