package com.harry.dsa;
class PNode{
	char data;
	PNode next;
}

public class ParenthesisCheck {
	static PNode top;
	static int maxStack = 10;
	static int elementCount = 0;
	static boolean isFull() {
		if(maxStack<=elementCount)
			return true;
		else
			return false;
	}
	
	static boolean isEmpty() {
		if(top==null)
			return true;
		else
			return false;
	}
	
	static void push(char value) {
		if(isFull()) {
			System.out.println("Stack is full");
		} else {	
		PNode node = new PNode();
		node.data = value;
		if(top==null) {
			top = node;
			elementCount++;
		}else {
			node.next = top;
			top=node;
			elementCount++;
		}
		}
	}
	
	static void pop() {
		if(isEmpty()) {
			System.out.println("No element in stack");
		}else {
			System.out.println(top.data);
			PNode temp = top.next;
			top.next=null;
			top =temp;
			--elementCount;
		}
		
	}
	
	static void peak(int position) {
		if(position<=0) {
			System.out.println("Not Valid postion");
		}else {
			PNode temp = top;
			for(int i=1;i<position;i++) {
				temp = temp.next;	
			}
			System.out.println(temp.data);
		}
		
	}

	public static void main(String[] args) {	
		
		String expression = "(8+(1+1)";
		char[] charExpre = expression.toCharArray();
		for(char c : charExpre) {
		if(c=='(') {
			push(c);
		}else if(')'==c) {
			pop();
		}
		
		}
		if(isEmpty()) {
			System.out.println("expression satisfied");
		}else {
			System.out.println("expression not satisfied");
		}
		


	}

}
