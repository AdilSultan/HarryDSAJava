package com.harry.dsa;
class PPINode{
	int data;
	PPINode left;
	PPINode right;
}

public class PrePostInOrderTraverseTree {
	
	static PPINode create(int value) {
		PPINode node = new PPINode();
		node.data = value;
		node.left = null;
		node.right = null;
		return node;
	}
	
	static void preOrder(PPINode root) {
		
		if(root!=null) {
		System.out.print(root.data);
		preOrder(root.left);
		preOrder(root.right);
		}
	}
	
	static void postOrder(PPINode root) {
		if(root!=null) {
			postOrder(root.left);
			postOrder(root.right);
		System.out.print(root.data);
		}
	}
	
	static void inOrder(PPINode root) {
		if(root!=null) {
			inOrder(root.left);
		System.out.print(root.data);
		inOrder(root.right);
		}
	}
	
	static boolean isBTS(PPINode root) {
		PPINode prev = null;
		if(root!=null) {
			if(!isBTS(root.left)) {
				return false;
			}
			if(prev!=null && root.data <= prev.data) {
				return false;
			}
			prev= root;
			return isBTS(root.right);
		}else {
		return true;
		}
	}

	public static void main(String[] args) {
		
		PPINode node1 = create(2);
		PPINode node2 = create(3);
		PPINode node3 = create(1);
		PPINode node4 = create(4);
		PPINode node5 = create(6);
		
		node1.left = node2;
		node1.right = node5;
		
		node2.left = node3;
		node2.right = node4;
		
		//preOrder(node1);
		inOrder(node1);
		
		System.out.println(isBTS(node1));
		
		
		
		
	}

}
