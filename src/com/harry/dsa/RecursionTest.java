package com.harry.dsa;

public class RecursionTest {
	
	static void get(int n) {
		if(n<1)
			return;
		get(n-1);
		get(n-3);
		System.out.print(n);
	}

	public static void main(String[] args) {
		get(6);
	}

}
