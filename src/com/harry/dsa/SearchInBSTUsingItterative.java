package com.harry.dsa;
import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

public class SearchInBSTUsingItterative {
	
	public static BNode create(int data) {
		BNode bnode = new BNode();
		bnode.data = data;
		bnode.left = null;
		bnode.right = null;
		return bnode;
	}
	
	public static BNode search(BNode root,int find) {
		
		if(root==null) {
			return null;
		}
		
		 while(root!=null) {
			 if(root.data==find) {
				 return root;
			 }
			 else if(root.data > find) {
				 root = root.left;
			 }else {
				 root = root.right;
			 }
		 }
		return null;
		
	}

	public static void main(String[] args) {
		
		BNode node1= create(50);
		BNode node2= create(40);
		BNode node3= create(20);
		BNode node4= create(45);
		BNode node5= create(60);
		BNode node6= create(55);
		BNode node7= create(70);
		
		node1.left = node2;
		node1.right = node5;
		
		node2.left = node3;
		node2.right = node4;
		
		node5.left = node6;
		node5.right = node7;
		
		BNode found = search(node1, 45);
		if(found!=null)
		System.out.println(found.data);
		else
			System.out.println("not found");

	}

}
