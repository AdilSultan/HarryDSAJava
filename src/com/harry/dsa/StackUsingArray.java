package com.harry.dsa;

public class StackUsingArray {
	static int capacity = 20;
	static int top = -1;
	static int[] stack = new int[capacity];
	
	static boolean isEmpty() {
		if(top==-1)
		return true;
		return false;
	}
	
	static boolean isFull() {
		if(top==capacity-1)
			return true;
		return false;
	}
	
	static void push(int value) {
		if(isFull()) {
			System.out.println("stack is full!");
		}else {
			stack[++top]=value;
		}
	}
	
	static void pop() {
		if(isEmpty()) {
			System.out.println("stack is empty");
		}else {
			System.out.println(stack[top]);
			stack[top] = 0;
			--top;
		}
	}
	
	static void peak(int index) {
		if(isEmpty()) {
			System.out.println("stack is emplty");
		}else {
			if(index <= top)
			System.out.println(stack[top - (index-1)]);
			else
			System.out.println("no data found at index "+index);
		}
	}

	public static void main(String[] args) {
		
		
		push(10);
		push(20);
		push(30);
		push(40);
		push(50);
		
		peak(4);
		
	}

}
