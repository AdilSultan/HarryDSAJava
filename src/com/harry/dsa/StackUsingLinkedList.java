package com.harry.dsa;
class SNode{
	int data;
	SNode next;
	
}

public class StackUsingLinkedList {
	static SNode top;
	static int maxStack = 10;
	static int elementCount = 0;
	static void push(int value) {
		if(maxStack<=elementCount) {
			System.out.println("Stack is full");
		} else {	
		SNode node = new SNode();
		node.data = value;
		if(top==null) {
			top = node;
			elementCount++;
		}else {
			node.next = top;
			top=node;
			elementCount++;
		}
		}
	}
	
	static void pop() {
		if(top==null) {
			System.out.println("No element in stack");
		}else {
			System.out.println(top.data);
			SNode temp = top.next;
			top.next=null;
			top =temp;
			--elementCount;
		}
		
	}
	
	static void peak(int position) {
		if(position<=0) {
			System.out.println("Not Valid postion");
		}else {
			SNode temp = top;
			for(int i=1;i<position;i++) {
				temp = temp.next;	
			}
			System.out.println(temp.data);
		}
		
	}

	public static void main(String[] args) {		
push(10);
push(20);
push(30);
push(40);
push(50);

peak(4);


	}

}
